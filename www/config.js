module.exports = new function(){

    /**
     * Site config
     */
    this.site = {
        title: "SUA",
        description: "Servicesenter for Utenlandske Arbeidstakere",
        author: "Creuna"
    };

    /**
     * Path config
     */
    this.path = {
        src: "./app/",
        dest: "./dist/",
        build: "./build/"
    };

    this.assets = {
        scripts: "js/",
        styles: "sass/"
    };

    this.layout = {
        data: "data/",
        components: "components/",
        helpers: "components/_helpers/",
        partials: "components/_partials/",
        templates: "components/templates/"
    };

    this.paths = {
        assets: {
            src: this.path.src + "assets/",
            dest: this.path.dest + "assets/",
            build: this.path.build + "assets/"
        },
        styles: {
            src: this.path.src + this.assets.styles,
            dest: this.path.dest + "css/",
            build: this.path.build + "css/"
        },
        scripts: {
            src: this.path.src + this.assets.scripts,
            dest: this.path.dest + "js/",
            build: this.path.build + "js/"
        },
        layout: {
            src: this.path.src + this.layout.templates,
            dest: this.path.dest,
            build: this.path.build,
            data: this.path.src + this.layout.data,
            components: this.path.src + this.layout.components,
            helpers: this.path.src + this.layout.helpers,
            partials: this.path.src + this.layout.partials,
            layouts: this.path.src + this.layout.layouts
        }
    };
}();