// > base
define(['vendor/handlebars'],  function(Handlebars){

	// > example
	function Base(){
	}

	Base.prototype.init = function(){};
	Base.prototype.afterInit = function(){};
	Base.prototype.destroy = function(){};

	Base.prototype.setTemplate = function(template){
		this._compiled = Handlebars.compile(template);
		
	};
	Base.prototype.renderTemplate = function(data){
		if(!this._compiled){
			return null;
		}

        return this._compiled({ data: data });
	};

	// > return the component
	return Base;
});