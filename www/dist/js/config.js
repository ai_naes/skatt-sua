requirejs.config({
	baseUrl: "js",
	paths: {
		// short links
		abstract:			'../components/abstract',
		atom:				'../components/atoms',
		molecule:			'../components/molecules',
		organism:			'../components/organisms',
		template:			'../components/templates',
		plugin:				'../components/plugins',
		ajax:				'../assets/ajax',

		vendor:				'./vendor',
		jquery:				'./vendor/jquery-2.1.3',
		text:				'./vendor/text',

		foundation: 				'./vendor/foundation',
		foundationEqualizer: 		'./vendor/foundation.equalizer'
	},
	shim: {
		foundation: ['jquery'],
		foundationEqualizer: ['jquery', 'foundation'],
		jquery: {
			exports: 'jquery'
		}
	}
});