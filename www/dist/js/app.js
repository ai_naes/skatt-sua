require([
	"jquery",
	"foundation",
	"foundationEqualizer"

], function(
	$,
	Foundation,
    foundationEqualizer
    ){


	$(document).foundation({
        equalizer : {
            equalize_on_stack: true
        }
    });

    
});